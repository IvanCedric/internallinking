
import json
import random
import sys
from collections import OrderedDict
import operator
import urllib.parse
import pymysql
from elasticsearch import Elasticsearch


# a.k.a Starting solution


class InternalLinkOutgoing:
    def __init__(self):
    
        self.mysql_db = None    # if mysql is preferred
        self.elasticsearch_db = None    # if elasticsearch is preferred
        
        self.keyword = ""
        self.location = ""
        self.sitemap_keywords = []
        
        self.type = "keyword"   # can be 'keyword' or 'keyword_location'
                                # default is 'keyword'
        
        self.terms = []     # list of significant terms of current keyword (different from self.one_words)
        
        self.one_words = []     # list of significant terms of current keyword that is also in sitemap,
                                # and should be a subset of self.terms
        self.two_words = []
        self.three_words = []
        self.four_words = []
        self.five_words = []
        
        self.locations = []     # locations in the sitemap
        
        self.wordlist = []      # list of keywords, classified by number of words
                                # classification can be modified based on linking scheme
        
        self.outgoing_links = {}    # outgoing_links[ keyword ] = number of links
        
        self.incoming_links = {}    # outgoing_links[ keyword ] = number of links
        
        self.search_vol = {}    # search_vol[ keyword ] = search volume
        
    def setup(self, sitemap_file="", locations_file='', searchvol_file="", outgoing_links_file=""):
        # set up the lists and databases to be used in the linking scheme
        
        if self.mysql_db is not None:
            self.cursor = self.mysql_db.cursor()
            
        if self.elasticsearch_db is not None:
            self.es = Elasticsearch(self.elasticsearch_db)
        
        with open(sitemap_file) as f:   # load sitemap
            file = f.readlines()
        print("Loading sitemap...")
        for line in file:
            line = line.strip('\n')
            self.sitemap_keywords.append(line)
            self.outgoing_links[ line ] = 0
        
        try:
            with open(locations_file) as f:     # load locations
                file = f.readlines()
        except FileNotFoundError:
            file=[]
        print("Loading locations...")
        for line in file:
            line = line.strip('\n')
            self.locations.append(line)
        
        try:
            with open(searchvol_file) as f:     # load search volumes
                file = f.readlines()
        except FileNotFoundError:
            file=[]
        print("Loading search volumes...")
        for line in file:
            line = line.strip('\n')
            line_split = line.split(',')
            self.search_vol[ line_split[0] ] = int( line_split[1] )
        
        try:
            with open(outgoing_links_file) as f:    # load outgoing links
                file = f.readlines()
        except FileNotFoundError:
            file=[]
        print("Loading outgoing links...")
        for line in file:
            line = line.strip('\n')
            line_split = line.split(',')
            self.outgoing_links[ line_split[0] ] = int( line_split[1] )
            
    def get_sig_terms(self):
        # returns sig_terms of the current keyword
        links = []
        
        return links
        
    def get_sig_keywords(self, limit=5, links=[]):
        # returns the sitemap keywords that are significant to current keyword
        count = 0
        sig_keywords = []
        
        for link in links:
            if count >= limit:
                break
            if link in self.sitemap_keywords:
                sig_keywords.append(link)
                count += 1
                
        return sig_keywords
        
    def point_to_links(self):
        # returns the final set of links that the current keyword will point to
        # only fill this up when specific number of words per keyphrase is needed
        # e.g. we need 4 2-words and 1 1-word
        pass
    
    def run(self, keyword="", type="keyword"):  #, type="keyword_location"):
    
        if type=="keyword":
        
            self.keyword = keyword
            self.location = None
            
        elif type=="keyword_location":
            
            key_loc = keyword.strip('\n')
            key_loc = key_loc.split("-jobs-in-")
            self.keyword = key_loc[0]
            self.location = key_loc[1]
            
        self.terms = self.get_sig_terms()
        return self.get_sig_keywords(links=self.terms)
            
    def run_all(self, link_file="", output_file="", no_result_file="", type="keyword_location"):
        
        self.type = type
        
        out = open(output_file, 'w')
        no_out = open(no_result_file, 'w')
        
        with open(link_file) as f:
            file = f.readlines()
        for line in file:
            line = line.strip('\n')
            
            result = self.run(line, self.type)
            if len(result) == 0:
                entry = line + '\n'
                no_out.write(entry)
            else:
                for link in result:
                    entry = line + ',' + link + '\n'
                    out.write(entry)
    

if __name__ == "__main__":
    print("Not meant to be run alone. Probably will not even run.")
    
    
    
    
            
            