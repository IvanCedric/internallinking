
import xmas_internal_linking_relevance_oneword_nosearch
import sys
import random
import os
from datetime import datetime


start=datetime.now()
print('Start: ', start) 

print("Processing... This may take a loooong time...")

###########THE ONEWORDS ######################
onewords_file = open("../ones_search/fill_one_words_after_one_searchvol.csv","r")
#Read the file and split it by line "\n"
#Compare to line.strip('\n')
#it is now an array
ones={}
onewords = onewords_file.read().splitlines()
for one in onewords:
    ones[one.split(",")[0]]=one.split(",")[1]
##############################################

#########PRIORITY #1: ONE WORDS WITH SEARCHVOL#####
print("Consider One Words with No Searchvol:")
f1 = open("results_xmas_one_nosearchvol.csv", 'w')
f2 = open("not_enough_results_xmas_one_nosearchvol.txt", 'w')

il_one= xmas_internal_linking_relevance_oneword_nosearch.InternalLinky()

count=0

for key in ones.keys():
    count += 1
    print("count: %d" % count)
    limit=10-int(ones[key])
    pointers = il_one.run(key,limit)

    for j in range(len(pointers)):
        f1.write(pointers[j]+ ',' + key + '\n')
    if len(pointers)==0:
        print ("Warning: No pointers for keyword: " + key )
        f2.write(key+ ',%d' % len(pointers) +'\n')
    il_one.clear()
f1.close()
f2.close()

f3 = open("incoming_link_xmas_one_nosearchvol.csv",'w')
for key in il_one.outgoing_link.keys():
    f3.write("%s,%d\n" % (key, il_one.outgoing_link[key]))
f3.close()


print('Done.')
end=datetime.now()
print('duration: ', (end-start))
print('End: ', end)