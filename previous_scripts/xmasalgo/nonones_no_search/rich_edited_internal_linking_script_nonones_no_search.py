import xmas_internal_linking_relevance_nononeword
import sys
import random
import os
from datetime import datetime


start=datetime.now()
print('Start: ', start) 

print("Processing... This may take a loooong time...")

###########THE nononeWORDS ######################
nononewords_file = open("../nonones_search/fill_nonone_words_after_nonone_searchvol.csv","r")
#Read the file and split it by line "\n"
#Compare to line.strip('\n')
#it is now an array
nonones={}
nononewords = nononewords_file.read().splitlines()
for nonone in nononewords:
    nonones[nonone.split(",")[0]]=nonone.split(",")[1]
##############################################

#########PRIORITY #1: nonone WORDS WITH SEARCHVOL#####
print("Consider nonone Words with Searchvol First:")
f1 = open("results_xmas_nonone_nosearchvol.csv", 'w')
f2 = open("not_enough_results_xmas_nonone_nosearchvol.txt", 'w')

il_nonone_nosearch = xmas_internal_linking_relevance_nononeword.InternalLinky()

count=0
for key in nonones.keys():
    count += 1
    print("count: %d" % count)
    limit=2-int(nonones[key])
    pointers = il_nonone_nosearch.run(key,limit)

    for j in range(len(pointers)):
        f1.write(pointers[j]+ ',' + key + '\n')
    if len(pointers)==0:
        print ("Warning: No pointers for keyword: " + key )
        f2.write(key+ ',%d' % len(pointers) +'\n')
    il_nonone_nosearch.clear()
f1.close()
f2.close()

f3 = open("incoming_link_xmas_nonone_nosearchvol.csv",'w')
for key in il_nonone_nosearch.outgoing_link.keys():
    f3.write("%s,%d\n" % (key, il_nonone_nosearch.outgoing_link[key]))
f3.close()


print('Dnonone.')
end=datetime.now()
print('duration: ', (end-start))
print('End: ', end)