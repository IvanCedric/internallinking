#start with ones_search
cd ones_search
python3 rich_edited_internal_linking_script_ones_search.py

cd ../nonones_search
python3 rich_edited_internal_linking_script_nonones_search.py

cd ../ones_no_search
python3 rich_edited_internal_linking_script_ones_no_search.py

cd ../nonones_no_search
python3 rich_edited_internal_linking_script_nonones_no_search.py
