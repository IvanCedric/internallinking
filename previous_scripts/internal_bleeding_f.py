
import json
import pymysql
import random
import urllib.parse
import sys

class InternalLinky:
    def __init__(self):
        self.db = pymysql.connect("localhost", "root", "requiem666", "elastic_dummy_2", charset='utf8')
        self.cursor = self.db.cursor()
        
        self.keyword = ""
        self.terms = []
        self.sitemap = "sitemap.txt"
        
        self.two_words = []
        self.three_words = []
        self.four_words = []
        self.five_words = []
        
        self.wordlist = []
        
        self.one_word_list = []
        with open(self.sitemap) as f:
            for line in f:
                if len(line.strip('\n').split(" ")) == 1:
                    self.one_word_list.append(line.strip('\n'))
                else:
                    break
        
        #self.links = []
        #print(self.one_word_list)

    def get_links(self, keyword):
        self.keyword = keyword
        terms = []
        if not len(keyword.split(" ")) <= 1:
            terms = keyword.strip('\n').split(" ")
        #print (keyword.title())
        #urllib.parse.quote(keyword).replace("%20"," ")
        self.cursor.execute('select significant_term from bucket_list_new where keyword="'+urllib.parse.quote(keyword.title()).replace("%20"," ")+'" and significant_term_type="title"')
        response = self.cursor.fetchall()
        #print (response)
        #exit()
        #print(type(response[1][0]))
        for k in response:
            #print(k)
            terms.append(k[0])
        #print(terms)
        for k in terms:
            #print(k)
            if k in self.one_word_list and keyword.lower() != k.lower() and k.lower() not in self.terms:
                self.terms.append(k)
        #print(self.terms)
        #self.terms = terms
        return self.terms
        
    def get_sitemap_keywords(self, terms):
        sitemap_keywords = []
        limit = len(self.keyword.split(" "))
        if limit < 2:
            limit = 2
        with open(self.sitemap) as f:
            for line in f:
                curr_keyword = line.strip('\n').split(" ")
                if(len(curr_keyword) < 2 or len(curr_keyword) > limit) or line.strip('\n').lower() == self.keyword.lower():
                    continue
                elif (not set(terms).isdisjoint(curr_keyword)):
                    #sitemap_keywords.append(line.strip('\n'))
                    if len(curr_keyword) == 2 and line.strip('\n') not in self.two_words:
                        self.two_words.append(line.strip('\n'))
                    elif len(curr_keyword) == 3 and line.strip('\n') not in self.three_words:
                        self.three_words.append(line.strip('\n'))
                    elif len(curr_keyword) == 4 and line.strip('\n') not in self.four_words:
                        self.four_words.append(line.strip('\n'))
                    elif len(curr_keyword) == 5 and line.strip('\n') not in self.five_words:
                        self.five_words.append(line.strip('\n'))
        #print(sitemap_keywords)
        #return sitemap_keywords
        #print (self.two_words)
        #print (self.three_words)
        #print (self.four_words)
        #print (self.five_words)
        
    def point_to_link(self):
        
        self.wordlist = [self.five_words, self.four_words, self.three_words, self.two_words, self.terms]
        
        def random_append(index):
            if len(self.wordlist[index]) <= 0:
                return random_append(index+1)
            return random.choice(self.wordlist[index])
            
        def remove_from_wordlist(index, word):
            try:
                self.wordlist[index].remove(word)
            except ValueError:
                remove_from_wordlist(index+1, word)
            except IndexError:
                pass
                
        
        links = []
        try:
            if len(self.keyword.split(" ")) == 5:
                links.append(random_append(0))
                links.append(random_append(1))
                links.append(random_append(2))
                #for i in range(2):
                #    temp = random.choice(self.two_words)
                #    links.append(temp)
                #    self.two_words.remove(temp)
                links.append(random_append(3))
                links.append(random_append(4))
            elif len(self.keyword.split(" ")) == 4:
                temp = random_append(1)
                links.append(temp)
                remove_from_wordlist(1, temp)
                links.append(random_append(1))
                links.append(random_append(2))
                #for i in range(2):
                #    temp = random.choice(self.two_words)
                #    links.append(temp)
                #    self.two_words.remove(temp)
                links.append(random_append(3))
                links.append(random_append(4))
            elif len(self.keyword.split(" ")) == 3:
                temp = random_append(2)
                links.append(temp)
                remove_from_wordlist(2, temp)
                temp = random_append(2)
                links.append(temp)
                remove_from_wordlist(2, temp)
                links.append(random_append(2))
                #for i in range(2):
                #    temp = random.choice(self.two_words)
                #    links.append(temp)
                #    self.two_words.remove(temp)
                links.append(random_append(3))
                links.append(random_append(4))
            elif len(self.keyword.split(" ")) == 2:
                for i in range(5):
                    temp = random_append(3)
                    links.append(temp)
                    remove_from_wordlist(3, temp)
            elif len(self.keyword.split(" ")) == 1:
                for i in range(3):
                    temp = random_append(3)
                    links.append(temp)
                    remove_from_wordlist(3, temp)
                for i in range(2):
                    temp = random_append(4)
                    links.append(temp)
                    remove_from_wordlist(4, temp)
        except IndexError:
            #print ("Warning: Not enough terms for keyword: " + self.keyword)
            pass
        #print (self.terms)
        #print (links)
        links = set(links)
        return list(links)
        
    def clear(self):
        self.keyword = ""
        self.two_words = []
        self.three_words = []
        self.four_words = []
        self.five_words = []
        self.terms = []
        
    def run(self, keyword):
        gl = self.get_links(keyword)
        sitemap_words = self.get_sitemap_keywords(gl)
        links = []
        links.append(self.point_to_link())
        links.append(self.point_to_link())
        
        for i in links[0]:
            if i in links[1]:
                links[1].remove(i)
        
        return links
        
    
#il = InternalLinky()   
#print(il.run("maler lackierer bauten korrosionsschutz")) 

#print(il.run("physiker"))
#t = il.get_links("ärztin")
#print (t)
#g = il.get_sitemap_keywords(t)
#print(g)
#print(il.point_to_link())
#il.run("sales manager anlagenbau")
#print(t)