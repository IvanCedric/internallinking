
import relevance_searchvol
import time

start = time.time()

il = relevance_searchvol.InternalLinky()

f1 = open("relevance_results_5.csv", 'w')
#f2 = open("no_results.txt", 'w')
f3 = open("relevance_results_10.txt", 'w')

print("Processing... This may take a loooong time...")

with open("sitemap.txt") as file:
    f = file.readlines()
    
for line in f:
    res = il.run(line.strip('\n'))

    for i in range(5):
        try:
            f1.write(line.strip('\n') + ',' + res[0][i] + '\n')
            f3.write(line.strip('\n') + ',' + res[0][i] + '\n')
            f3.write(line.strip('\n') + ',' + res[1][i] + '\n')
        except IndexError:
            #print ("Warning: Not enough terms for keyword: " + line.strip('\n'))
            #f2.write(line)
            pass
            
    il.clear()
        
f1.close()
f3.close()

print ("--- %s seconds ---" % (time.time() - start))