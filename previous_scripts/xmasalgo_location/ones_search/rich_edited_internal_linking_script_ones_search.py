
import xmas_internal_linking_relevance_oneword
import sys
import random
import os
from datetime import datetime


start=datetime.now()
print('Start: ', start) 

print("Processing... This may take a loooong time...")

searchvol_file="../searchvol_123words.csv"
#######SEARCHVOLFIRST########################
keywords_with_searchvol_oneword=[]
tmplines=open(searchvol_file, "r")
searchvol=tmplines.read().splitlines()
for key_with_searchvol in searchvol:
    key_vol=key_with_searchvol.split(',')
    if len(key_vol[0].split(" "))==1:
        keywords_with_searchvol_oneword.append(key_vol[0])
##############################################

###########THE ONEWORDS ######################
onewords_file = open("../fill_one_words.csv","r")
#Read the file and split it by line "\n"
#Compare to line.strip('\n')
#it is now an array
ones={}
onewords = onewords_file.read().splitlines()
for one in onewords:
    ones[one.split(",")[0]]=one.split(",")[1]
##############################################

random.shuffle(keywords_with_searchvol_oneword)
#########PRIORITY #1: ONE WORDS WITH SEARCHVOL#####
print("Consider One Words with Searchvol First:")
f1 = open("results_xmas_one_searchvol.csv", 'w')
f2 = open("not_enough_results_xmas_one_searchvol.txt", 'w')

il_one_search = xmas_internal_linking_relevance_oneword.InternalLinky()

count=0
for key in keywords_with_searchvol_oneword:
    if key in ones.keys():
        count += 1
        print("count: %d" % count)
        pointers = il_one_search.run(key)

        for j in range(len(pointers)):
            f1.write(pointers[j]+ ',' + key + '\n')
        if len(pointers)==0:
            print ("Warning: No pointers for keyword: " + key )
            f2.write(key+ ',%d' % len(pointers) +'\n')
        ones.pop(key)

    il_one_search.clear()
f1.close()
f2.close()

f3 = open("incoming_link_xmas_one_searchvol.csv",'w')
for key in il_one_search.outgoing_link.keys():
    f3.write("%s,%d\n" % (key, il_one_search.outgoing_link[key]))
f3.close()

f4 = open("fill_one_words_after_one_searchvol.csv",'w')
for key in ones.keys():
    f4.write("%s,%s\n" % (key, ones[key]))
f4.close()


print('Done.')
end=datetime.now()
print('duration: ', (end-start))
print('End: ', end)