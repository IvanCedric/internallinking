cat ../results_5_sig_terms.csv ones_search/results_xmas_one_searchvol.csv nonones_search/results_xmas_nonone_searchvol.csv ones_no_search/results_xmas_one_nosearchvol.csv nonones_no_search/results_xmas_nonone_nosearchvol.csv | sort | uniq > results_all.csv

#outgoing links
echo "outgoing links: "
cut -d',' -f'1' results_all.csv | sort | uniq | wc -l

echo "incoming links: "
cut -d',' -f'2' results_all.csv | sort | uniq | wc -l

cd ../results_christmas_algo_1
ln -s ../xmasalgo/results_all.csv results.csv