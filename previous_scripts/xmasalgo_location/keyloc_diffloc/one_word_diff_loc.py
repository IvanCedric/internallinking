
import json
import pymysql
import random
import urllib.parse
import sys
from  collections import OrderedDict
import operator

class InternalLinky:
    def __init__(self):
        self.db = pymysql.connect("localhost", "root", "requiem666", "elastic_dummy_2", charset='utf8')
        self.cursor = self.db.cursor()
        self.newsql="bucket_list_for_new_sitemap"
        
        self.keyword = ""
        self.location = ""
        self.terms = []
        self.sitemap = "key-loc_utf8_lower-summary-pointer.csv"
        #self.searchvol = "../searchvol_123words.csv"

        self.two_words = []
        self.three_words = []
        self.four_words = []
        self.five_words = []
        
        self.wordlist = []
        
        self.sitemap_list = []
        self.one_word_list = []
        
        self.other_terms = []
        
        self.outgoing_link={}
        self.outgoing_link_threshold=7

        self.keywords_with_searchvol={}

        self.order_for_rand_append=1
        
        self.location_dict = {}     # location_dict[location] = [ (keyword, url_form), ... ]
        
        self.raw_keyword = ""
        
        self.location_agg = []
        
        
        # self.keyword_hit_list={}
        # with open("sitemap_hits_sorted.csv") as f:   #   need file   ####################
        #     file = f.readlines()
            
        # for i in file:
        #     temp = i.split(',')
        #     self.keyword_hit_list[ temp[0].strip() ] = int(temp[1].strip())
        
        with open("locations_in_sitemap.txt") as f:
            file = f.readlines()
        for aline in file:
            loc_temp = aline.strip('\n')
            #loc_temp = urllib.parse.quote(loc_temp).lower()
            loc_temp = loc_temp.encode('latin1').decode('utf8')
            self.location_dict[loc_temp] = []

        with open(self.sitemap) as f:
            file = f.readlines()
        
        for aline in file:
            tmp=aline.strip('\n').split(',')
            line=tmp[0]
            #if len(line.strip('\n').split(" ")) == 1:
            #    self.one_word_list.append(line.strip('\n'))
            #    self.sitemap_list.append(line.strip('\n'))
            #else:
            templine = line.split("-jobs-in-")
            keyword = ' '.join(templine[0].split('-'))
            #print (keyword)
            #exit()
            #location = urllib.parse.quote( templine[1] )
            location = templine[1]
            if location in self.location_dict.keys():
                self.location_dict[location].append( keyword )
            
            self.sitemap_list.append(line.strip('\n'))
            self.outgoing_link['%s' % (line.strip('\n'))]=int(tmp[1])
            
        
        #tmplines=open(self.searchvol, "r")
        #searchvol=tmplines.read().splitlines()
        #for key_with_searchvol in searchvol:
        #    key_vol=key_with_searchvol.split(',')
        #    self.keywords_with_searchvol[key_vol[0]]=key_vol[1]

    
    def query_title(self,table,terms,toparseornot):
        
        
        query='select keyword from '+ table +' where significant_term="'+self.keyword+'" and significant_term_type="title"'+'order by doc_count desc'
        #print("query: ", query)
        self.cursor.execute(query)

        response = self.cursor.fetchall()
        #print(self.outgoing_link.keys())
        #exit()

        #for k in response:
        #    if toparseornot==0:
        #        key=k[0].lower()
        #    else:
        #        key=urllib.parse.unquote(k[0]).lower()
            

        temp_res = []
        location_pool = {}
        for loc in self.location_agg:
            #temp_loc = ( loc, self.location_dict[loc] )
            try:
                for keyloc in self.location_dict[loc]:
                    #print(keyloc)
                    location_pool[ keyloc ] = loc
            except KeyError:
                print("Location '"+loc+"' does not exist. (UTF-8 error?)")
        
        for k in response:
            #print(k[0])
            if k[0].lower() in location_pool.keys():
                temp_k = k[0].lower() + "-jobs-in-" + location_pool[ k[0].lower() ]
                temp_res.append(temp_k)
        #print(temp_res)
        
        response = temp_res
        
        count=0
        for key in response:
            #key=urllib.parse.unquote(k[0]).lower()
            #print(key)
            if key not in terms and key != self.keyword and key in self.outgoing_link.keys():# and count < 5:
                if self.outgoing_link[key]< self.outgoing_link_threshold:
                    #print(key)
                    terms.append(key)
                    count += 1
            
            #temp_url = '-'.join(key.split(' '))
            #temp_url = temp_url + "-jobs-in-" + self.location
            #if key not in terms and key != self.keyword and temp_url in self.outgoing_link.keys():
            #    if self.outgoing_link[temp_url]< self.outgoing_link_threshold:
            #        terms.append(key)
                    
                    
        #print(terms)
        return terms

    def get_keywords(self):
        terms = []

        #terms = self.query_title('bucket_list_new',terms,0)
        
        
        
        self.cursor.execute('select aggregation from location_aggregation_for_rich where keyword="'+self.raw_keyword.strip('\n')+'" and hits>=10')
        
        loc_response = self.cursor.fetchall()
        
        for loc in loc_response:
            locy = loc[0].encode('utf8').decode('utf8')
            print(loc[0])
            locy = '-'.join( locy.split(' ') ).lower()
            if locy != self.location:
                self.location_agg.append(locy)
                
        terms = self.query_title('bucket_list_new',terms,0)
        

        if len(terms) == 0:
            #print("Keyword %s considered description: " % self.keyword)
            self.cursor.execute('select keyword from bucket_list_new where significant_term="'+self.keyword+'" and significant_term_type="description"'+'order by doc_count desc')

            response = self.cursor.fetchall()
            
            #print(type(response[0]))
            #if self.location not in self.location_dict.keys():
                #print('aha')
            #    return []
                
            temp_res = []
            location_pool = {}
            for loc in self.location_agg:
                #temp_loc = ( loc, self.location_dict[loc] )
                try:
                    for keyloc in self.location_dict[loc]:
                    #print(keyloc)
                        location_pool[ keyloc ] = loc
                except KeyError:
                    pass
                    
            
            for k in response:
                #print(k[0])
                if k[0].lower() in location_pool.keys():
                    temp_k = k[0].lower() + "-jobs-in-" + location_pool[ k[0].lower() ]
                    temp_res.append(temp_k)
            #print(temp_res)
            
            response = temp_res
            
            count=0
            for key in response:
                #key=urllib.parse.unquote(k[0]).lower()
                #print(key)
                if key not in terms and key != self.keyword and key in self.outgoing_link.keys():# and count < 5:
                    if self.outgoing_link[key]< self.outgoing_link_threshold:
                        #print(key)
                        terms.append(key)
                        count += 1
        #print(terms)
        return terms

    def clear(self):
        self.keyword = ""
        self.location = ""
        self.two_words = []
        self.three_words = []
        self.four_words = []
        self.five_words = []
        self.terms = []
        self.other_terms = []
        
        self.location_agg = []
        self.raw_keyword = ""
        
    def run(self, keyword,limit):
        print("keyword: %s" % keyword)  
        
        self.raw_keyword = keyword
        raw_key = keyword.split("-jobs-in-")
        
        self.keyword = " ".join(raw_key[0].split('-')).strip('\n')     
        self.location = raw_key[1].strip('\n')
        #self.location = urllib.parse.quote( self.location )
        
        #print(self.location_dict.keys())
        links = self.get_keywords()

        if keyword in links:
            index = links.index(keyword)
            links.pop(index)

        toPass=[]
        count=0
        for link in links:
            if count < limit:
                #temp_url = ('-'.join(link.split(' '))) + "-jobs-in-" + self.location
                try:
                    self.outgoing_link[link]+=1            
                    count += 1
                    toPass.append(link)
                except KeyError:
                    print("Keyword '"+link+"' does not exist.")

        self.clear()
        return toPass
    
    def run_all(self):
        
        f1 = open("xmas_algo_key_loc_nosearch.txt", 'w')
        f2 = open("xmas_algo_key_loc_nosearch_missing.txt", 'w')
        
        with open("missing_one_word.txt") as f:
            file = f.readlines()
        for keyloc in file:
            keyloc = keyloc.strip('\n')
            result = il.run(keyloc, 10)
            if len(result) <= 0:
                keyloc = keyloc + '\n'
                f2.write(keyloc)
            else:
                for link in result:
                    link_line = link + ',' + keyloc + '\n'
                    f1.write(link_line)
                    
        #f1.close()
        #f2.close()
    
il = InternalLinky()

il.run_all()
#t = urllib.parse.quote("böblingen")
#print(il.location_dict[ urllib.parse.quote(t).lower() ])

#print(il.location_dict.keys())

#print(il.run("ingenieur-jobs-in-stuttgart", 10))

#print (il.run("aas-jobs-in-duisburg", 10))
# print("====================\n\n")
# print("\n",il.run("elektrik",2))
# print("\n",il.run("umweltwissenschaften"))
# #print(t)
# pka,170
# pharmaberater,170
# umweltwissenschaften,170
# cfd,170
# medizinprodukteberater,170
# derma,170
# research,140
# java,140
# medizin,140
# labor,140
# linux,140
# programmierer,140
# python,140