
import json
import pymysql
import random
import urllib.parse
import sys
from  collections import OrderedDict
import operator

class InternalLinky:
    def __init__(self):
        self.db = pymysql.connect("localhost", "root", "", "Linking_Keyword_Loc", charset='utf8')
        self.cursor = self.db.cursor()
        self.newsql="bucket_list_for_new_sitemap"
        
        self.keyword = ""
        self.terms = []
        self.sitemap = "../ones_search/incoming_link_xmas_one_searchvol.csv"
        self.searchvol = "../searchvol_123words.csv"

        self.two_words = []
        self.three_words = []
        self.four_words = []
        self.five_words = []
        
        self.wordlist = []
        
        self.sitemap_list = []
        self.one_word_list = []
        
        self.other_terms = []
        
        self.outgoing_link={}
        self.outgoing_link_threshold=7

        self.keywords_with_searchvol={}

        self.order_for_rand_append=1
        
        # self.keyword_hit_list={}
        # with open("sitemap_hits_sorted.csv") as f:   #   need file   ####################
        #     file = f.readlines()
            
        # for i in file:
        #     temp = i.split(',')
        #     self.keyword_hit_list[ temp[0].strip() ] = int(temp[1].strip())

        with open(self.sitemap) as f:
            file = f.readlines()
        
        for aline in file:
            tmp=aline.strip('\n').split(',')
            line=tmp[0]
            if len(line.strip('\n').split(" ")) == 1:
                self.one_word_list.append(line.strip('\n'))
                self.sitemap_list.append(line.strip('\n'))
            else:
                self.sitemap_list.append(line.strip('\n'))
            self.outgoing_link['%s' % (line.strip('\n'))]=int(tmp[1])
        
        tmplines=open(self.searchvol, "r")
        searchvol=tmplines.read().splitlines()
        for key_with_searchvol in searchvol:
            key_vol=key_with_searchvol.split(',')
            self.keywords_with_searchvol[key_vol[0]]=key_vol[1]

    def query_title(self,table,terms,toparseornot):

        query='select significant_term from '+ table +' where keyword="'+self.keyword+'" and significant_term_type="title"'
        #print("query: ", query)
        self.cursor.execute(query)

        response = self.cursor.fetchall()

        for k in response:
            if toparseornot==0:
                key=k[0].lower()
            else:
                key=urllib.parse.unquote(k[0]).lower()
            if key not in terms and key != self.keyword and key in self.outgoing_link.keys():
                if self.outgoing_link[key]< self.outgoing_link_threshold:
                    terms.append(key)

        return terms

    def get_links(self, keyword, sig_term_type):
        self.keyword = keyword
        terms = []

        #if not one-word
        #if not len(keyword.split(" ")) <= 1:
        terms = keyword.strip('\n').split(" ")

        if sig_term_type=="title":

            self.cursor.execute('select significant_term from bucket_list_new where keyword="'+urllib.parse.quote(keyword.title()).replace("%20"," ")+'" and significant_term_type="'+sig_term_type+'"')

            response = self.cursor.fetchall()

            for k in response:
                if k[0] not in terms and k[0].islower():
                    terms.append(k[0])


            self.cursor.execute('select significant_term from '+self.newsql +' where keyword="'+keyword.title()+'" and significant_term_type="title"')

            response = self.cursor.fetchall()

            for k in response:
                if k[0] not in terms and k[0].islower():
                    terms.append(k[0])

        elif sig_term_type=="terms_agg":
            tmp='select significant_term from '+self.newsql +' where keyword="'+urllib.parse.quote(keyword.title()).replace("%20"," ")+'" and significant_term_type="title"'
            self.cursor.execute(tmp)

            response = self.cursor.fetchall()

            for k in response:
                if k[0] not in terms and k[0].islower():
                    terms.append(k[0])
        
        temp=[t for t in self.other_terms]
        self.other_terms = temp+[t for t in terms if t.islower() and t not in temp]
        if len(keyword.split(" ")) == 1 and keyword not in self.other_terms:
            self.other_terms.append(keyword)
        
        for k in terms:
            #print(k)
            #if k in self.one_word_list and keyword.lower() != k.lower() and k.lower() not in self.terms and self.outgoing_link[k] < self.outgoing_link_threshold:
            if k in self.one_word_list and k.lower() not in self.terms and k in self.outgoing_link.keys():
                if self.outgoing_link[k] < self.outgoing_link_threshold:
                    self.terms.append(k)
        print("self.other_terms upon exit of get_links() having %s as sig_term_type" % sig_term_type, self.other_terms)

        #self.terms = terms

        if len(self.other_terms)<=5 and sig_term_type=="title":
                return self.get_links(keyword,"title")

        print("self.terms upon exit of get_links() having %s as sig_term_type: " % sig_term_type, self.terms)

        return self.terms
        
    def get_sitemap_keywords(self, terms):
        sitemap_keywords = []
        # limit = len(self.keyword.split(" "))
        # if limit < 2:
        #     limit = 2
        limit=5
        #with open(self.sitemap) as file:
        #    f = file.readlines()
        #print ("length of sitemap:", len(self.sitemap_list))
        for line in self.sitemap_list:
            curr_keyword = line.strip('\n').split(" ")

            if self.outgoing_link[line.strip('\n')] == self.outgoing_link_threshold:
                continue

            if(len(curr_keyword) < 2 or len(curr_keyword) > limit) or line.strip('\n').lower() == self.keyword.lower():
                continue

            elif (not set(self.other_terms).isdisjoint(curr_keyword)):
                    #sitemap_keywords.append(line.strip('\n'))
                if len(curr_keyword) == 2 and line.strip('\n') not in self.two_words:
                    self.two_words.append(line.strip('\n'))
                elif len(curr_keyword) == 3 and line.strip('\n') not in self.three_words:
                    self.three_words.append(line.strip('\n'))
                elif len(curr_keyword) == 4 and line.strip('\n') not in self.four_words:
                    self.four_words.append(line.strip('\n'))
                elif len(curr_keyword) == 5 and line.strip('\n') not in self.five_words:
                    self.five_words.append(line.strip('\n'))
        #print(sitemap_keywords)
        #return sitemap_keywords
        #print (self.two_words)
        #print (self.three_words)
        #print (self.four_words)
        #print (self.five_words)
        self.wordlist = [self.five_words, self.four_words, self.three_words, self.two_words, terms]
        return self.wordlist        

    def sort_based_on_hits(self,alist):
        orderedlist=[]
        sorted_x = sorted(self.keyword_hit_list.items(), key=operator.itemgetter(1))
        for key,value in sorted_x:
            if key in alist:
                orderedlist.append(key)
        return orderedlist


    def get_relkey(self,limit):

        #print("IN get_relkey_for_keyloc_linking:")

        print("keyword is %s" % self.keyword)

        #get the length of the keyword
        len_key = len(self.keyword.split(" "))
        #print("length of keyword: %s" % len_key)

        #print sig_terms
        # print("other_terms:")
        # print(self.other_terms)


        #get union of all related keywords in sitemap
        keylinks = list(set(self.five_words) | set(self.four_words) | set(self.three_words) | set(self.two_words) | set(self.terms))
        #print("keylinks: ")
        #print(keylinks)

        #create relevant files
        #for i in range(len_key,0,-1):
        #    for j in range(len_key):

        candidates={}
        
        ############################################
        ##### assume we always have 5-word candidates
        ############################################
        len_key=5

        for i in range(1,len_key+1):
            for j in range(len_key):
                if (i+j) <= len_key:
                    candidates['%d%d' %(i,j)]=[]

        #loop over those in keylinks
        for key in keylinks:
            #print("key: %s" % key)
            keyterms = key.split(" ")
            num_sig=len(list(set(keyterms) & set(self.other_terms)))
            num_not_sig= len(keyterms)-num_sig
            if (num_sig>=num_not_sig) and len(list(set(keyterms) & set(self.terms)))>1 and len(keyterms)>=len(self.keyword.split(" ")):
                keyDict="%d%d" %(num_sig,num_not_sig)
                candidates[keyDict].append(key)
            # afile = "%s/%d%d.txt" % (self.keyword,num_sig,num_not_sig)
            # f=open(afile,'a')
            # f.write(key+"\n")
            #words = keyword.split(" ")

        #print("\n\ncandidates")
        #print(candidates)

        #randomize each list
        for i in range(1,len_key+1):
            for j in range(len_key):
                if (i+j) <= len_key:
                    #self.sort_based_on_hits(candidates['%d%d' %(i,j)])
                    random.shuffle(candidates['%d%d' %(i,j)])

        #print("\n\nrandomized candidates")
        #print(candidates)


        links=[]
        #write to file
        counter=0
        for j in range(len_key):
            for i in range(len_key,0,-1):
                if (i+j) <= len_key:
                    for key in candidates['%d%d' %(i,j)]:
                        if counter==limit:
                            return links 
                        links.append(key)
                        counter=counter+1

        return links
        
    def clear(self):
        self.keyword = ""
        self.two_words = []
        self.three_words = []
        self.four_words = []
        self.five_words = []
        self.terms = []
        self.other_terms = []
        
    def run(self, keyword, limit):
        print("keyword: %s" % keyword)
        gl = self.get_links(keyword,"terms_agg")
        
        #if len(gl)==0:
        #    gl = self.other_terms
        sitemap_words = self.get_sitemap_keywords(gl)

        links = []

        if keyword in self.terms:
            index = self.terms.index(keyword)
            self.terms.pop(index)            

        links = self.get_relkey(limit)

        for link in links:
            self.outgoing_link[link]+=1            
            

#        links.append(self.point_to_link())

#        for i in links[0]:
#            if i in links[1]:
#                links[1].remove(i)

        self.clear()
        return links
        
    
# il = InternalLinky()
# inverse = [(value, key) for key, value in il.outgoing_link.items()]
# print(max(inverse)[0])

# clist=['oracle soa suite', 'conversion rate optimization','oracle soa suite']
# for those in clist:
#     print("====================")
#     print("keyword is :", those, " having length ", len(those.split(" ")))
#     print("====================\n")
#     res=il.run(those,1000)
#     print("\nResult0:\n\t", res, "length: ", len(res))

# inverse = [(value, key) for key, value in il.outgoing_link.items()]
# print(max(inverse)[0])
# #t = il.get_links("ärztin")
# #print (t)
# #g = il.get_sitemap_keywords(t)
# #print(g)
# #print(il.point_to_link())
# print("====================\n\n")
# print("\n",il.run("sales manager anlagenbau"))

# print("====================\n\n")
# print("\n",il.run("antigenen",2))

# print("====================\n\n")
# print("\n",il.run("technical sales support service engineer"))

# print("====================\n\n")
# print("\n",il.run("wind energy project manager"))

# print("====================\n\n")
# print("\n",il.run("english junior project manager"))

# print("====================\n\n")
# print("\n",il.run("sap"))

# print("====================\n\n")
# print("\n",il.run("engineer"))

# print("====================\n\n")
# print("\n",il.run("chemical engineer"))

# print("====================\n\n")
# print("\n",il.run("mechanical engineer"))

# print("====================\n\n")
# print("\n",il.run("electrical engineer"))
# print("====================\n\n")
# print("\n",il.run("algorithmen c matlab"))

# print("====================\n\n")
# print("\n",il.run("3d game engine"))

# #print(t)