
import xmas_internal_linking_relevance_nononeword
import sys
import random
import os
from datetime import datetime


start=datetime.now()
print('Start: ', start) 

print("Processing... This may take a loooong time...")

searchvol_file="../searchvol_123words.csv"
#######SEARCHVOLFIRST########################
keywords_with_searchvol_nononeword=[]
tmplines=open(searchvol_file, "r")
searchvol=tmplines.read().splitlines()
for key_with_searchvol in searchvol:
    key_vol=key_with_searchvol.split(',')
    if len(key_vol[0].split(" "))>1:
        keywords_with_searchvol_nononeword.append(key_vol[0])
##############################################

###########THE nononeWORDS ######################
nononewords_file = open("../fill_twoormore_words.csv","r")
#Read the file and split it by line "\n"
#Compare to line.strip('\n')
#it is now an array
nonones={}
nononewords = nononewords_file.read().splitlines()
for nonone in nononewords:
    nonones[nonone.split(",")[0]]=nonone.split(",")[1]
##############################################

random.shuffle(keywords_with_searchvol_nononeword)
#########PRIORITY #1: nonone WORDS WITH SEARCHVOL#####
print("Consider nonone Words with Searchvol First:")
f1 = open("results_xmas_nonone_searchvol.csv", 'w')
f2 = open("not_enough_results_xmas_nonone_searchvol.txt", 'w')

il_nonone_search = xmas_internal_linking_relevance_nononeword.InternalLinky()

count=0
for key in keywords_with_searchvol_nononeword:
    if key in nonones.keys():
        count += 1
        print("count: %d" % count)
        unli=1000
        pointers = il_nonone_search.run(key,unli)

        for j in range(len(pointers)):
            f1.write(pointers[j]+ ',' + key + '\n')
        if len(pointers)==0:
            print ("Warning: No pointers for keyword: " + key )
            f2.write(key+ ',%d' % len(pointers) +'\n')
        nonones.pop(key)

    il_nonone_search.clear()
f1.close()
f2.close()

f3 = open("incoming_link_xmas_nonone_searchvol.csv",'w')
for key in il_nonone_search.outgoing_link.keys():
    f3.write("%s,%d\n" % (key, il_nonone_search.outgoing_link[key]))
f3.close()

f4 = open("fill_nonone_words_after_nonone_searchvol.csv",'w')
for key in nonones.keys():
    f4.write("%s,%s\n" % (key, nonones[key]))
f4.close()


print('Dnonone.')
end=datetime.now()
print('duration: ', (end-start))
print('End: ', end)