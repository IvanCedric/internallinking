import java.io.*;
import java.util.*;
import java.net.*;

public class Convert
{
	public static void main(String[] args) throws IOException 
	{
		String input_file = "locations_in_sitemap.txt";
		String output_file = "locations_in_sitemap_utf8_lower.txt";

		BufferedReader br = new BufferedReader(new FileReader(input_file));
		String current_line;
		PrintWriter writer = new PrintWriter(output_file, "UTF-8");
		while ((current_line = br.readLine()) != null) {
			current_line = URLDecoder.decode(current_line, "UTF-8").toLowerCase();
			writer.println(current_line);
		}
		writer.close();
	}
}